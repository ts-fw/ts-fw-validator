import {Provider} from 'ts-fw';
import {Validator} from 'class-validator';

@Provider('validatorService')
export class ValidatorProvider {
    provide(): Validator {
        return new Validator();
    }
}
